
-- SUMMARY --

AJAX Select can enable CCK select fields and/or single select taxonomy
vocabularies with an AJAX feature to dynamically update the value of that field
when viewing the node. It works by replacing the value of the field on the node
view by a select field.

-- REQUIREMENTS --

* CCK or Taxonomy

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Visit http://your-site/admin/settings/ajax-select

* Choose the content types, vocabularies and/or CCK fields which you want to
  enable the ajax select box for.

* Optionally enable the css coming with this module.

-- CUSTOMIZATION --

* Theme functions:
  theme_ajax_select_vocab_html($node, $term)
  theme_ajax_select_vocab_form($form)
  theme_ajax_select_field_html($node, $field, $delta, $node_field)
  theme_ajax_select_field_form($form)

-- CREDITS & CONTACT --

Current maintainer:

Makara Wang - http://drupal.org/user/132402

This project has been sponsored by:

Raincity Studios - http://www.raincitystudios.com/