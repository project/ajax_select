
/**
 * Without the submit button
 */
Drupal.ajaxSelectAutoAttach = function () {
  $('div.ajax-select').each(function () {
    var idBase = this.id;
    var uriId = '#' + idBase + '-uri';
    var uri = $(uriId).val();
    // On change
    var fieldId = '#'+ idBase +'-select';
    $(fieldId).change(function() {
      var selectValue = $(this).val();
      // Ajax GET request
      $.ajax({
        type: "GET",
        url: uri +'/'+ Drupal.encodeURIComponent(selectValue),
        beforeSend: function (xmlhttp) {
          $('#'+idBase).addClass("ajax-select-form-image");
        },
        success: function (data) {
          var response = Drupal.parseJson(data);
          if (response.status == 0) {
            alert('An error occurred:\n\n'+ response.data);
          }
          $('#'+idBase).removeClass("ajax-select-form-image");
        },
        error: function (xmlhttp) {
          alert('An HTTP error '+ xmlhttp.status +' occured.');
          $('#'+idBase).removeClass("ajax-select-form-image");
        }
      });
    });
  });
}


Drupal.ajaxSelectReplaceTerm = function () {
  $('div.ajax-select-vocab').each(function () {
    var idBase = this.id;
    var tidId = '#' + idBase + '-tid';
    var tid = $(tidId).val();
    // Replace
    $('a.taxonomy_term_'+tid).each(function () {
      var formHtml = $('#'+idBase+'-form').get();
      $(this).after(formHtml);
      $(this).remove();
    });
  });
}

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.ajaxSelectAutoAttach);
  //$(document).ready(Drupal.ajaxSelectReplaceTerm);
}
