<?php

/** 
 * @file
 * Provides CCK nodes with a select field with the ability to display
 * an AJax capable select field on view, for dynamically update the values.
 */

/**
 * Implementation of hook_menu().
 */
function ajax_select_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/ajax-select',
      'title' => t('Ajax Select'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('ajax_select_admin_settings'),
      'access' => user_access('administer site configuration'),
      'type' => MENU_NORMAL_ITEM
    );
    $items[] = array(
      'path' => 'ajax-select-field-submit',
      'title' => t('Ajax Select'),
      'callback' => 'ajax_select_field_submit',
      'access' => user_access('access content'),
      'type' => MENU_CALLBACK
    );
    $items[] = array(
      'path' => 'ajax-select-vocab-submit',
      'title' => t('Ajax Select'),
      'callback' => 'ajax_select_vocab_submit',
      'access' => user_access('access content'),
      'type' => MENU_CALLBACK
    );
  }
  return $items;
}

/**
 * Ajax callback
 * 
 * Callback function called through ajax to update the select field value.
 *
 * @param $nid
 *   Node id of the node to be updated
 * 
 * @param $field_name
 *   Name of the field to update (CCK)
 * 
 * @param $delta
 *   Field widget delta (CCK)
 * 
 * @param $select_value
 *   Selected value
 */
function ajax_select_field_submit($nid, $field_name, $delta = 0, $select_value = NULL) {
  $node = node_load($nid);
  if (empty($node)) {
    $status = FALSE;
    $output = t('not found');
  }
  elseif (!empty($select_value)) {
    // TODO: check option
    $node->{$field_name}[$delta]['value'] = $select_value;
    node_save($node);
    $status = TRUE;
    $output = $node->{$field_name}[$delta]['value'];
  }
  else {
    $status = FALSE;
    $output = t('wrong value');
  }
  print drupal_to_js(array('status' => $status, 'data' => $output));
  exit;
}

/**
 * Ajax callback
 * 
 * Callback function called through ajax to update the select taxonomy value.
 *
 * @param $nid
 *   Node id of the node to be updated
 * 
 * @param $tid
 *   Tid of the original term
 * 
 * @param $select_value
 *   Selected value
 */
function ajax_select_vocab_submit($nid, $vid, $select_value = NULL) {
  $node = node_load($nid);
  if (empty($node)) {
    $status = FALSE;
    $output = t('not found');
  }
  elseif (!empty($select_value)) {
    // TODO: check option
    foreach ($node->taxonomy as $tid => $term) {
      if ($term->vid == $vid) {
        unset($node->taxonomy[$tid]);
      }
    }
    $node->taxonomy[$vid] = $select_value;
    node_save($node);
    $status = TRUE;
    $output = $node->taxonomy[$select_value];
  }
  else {
    $status = FALSE;
    $output = t('wrong value');
  }
  print drupal_to_js(array('status' => $status, 'data' => $output));
  exit;
}

/**
 * Implementation of hook_nodeapi().
 */
function ajax_select_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'alter':
      // Taxonomy
      $tax_types = variable_get('ajax_select_tax_types', array());
      $tax_vocabs = variable_get('ajax_select_tax_vocabs', array());
      if (in_array($node->type, $tax_types) && !empty($tax_vocabs)) {
        foreach ($node->taxonomy as $tid => $term) {
          if (in_array($term->vid, $tax_vocabs)) {
            _ajax_select_add_header();
            _ajax_select_prepare_vocab_html($node, $term, $a3, $a4);
          }
        }
      }
      // CCK
      $cck_types = variable_get('ajax_select_cck_types', array());
      $cck_fields = variable_get('ajax_select_cck_fields', array());
      if (in_array($node->type, $cck_types) && !empty($cck_fields)) {
        foreach ($cck_fields as $field_name) {
          if (!empty($node->{$field_name})) {
            _ajax_select_add_header();
            _ajax_select_replace_field_html($node, $field_name, $a3, $a4);
          }
        }
      }
      break;
  }
}

function _ajax_select_add_header($force = FALSE) {
  static $added = FALSE;
  if (!$added || $force) {
    drupal_add_js(drupal_get_path('module', 'ajax_select') .'/ajax_select.js');
    $configs = variable_get('ajax_select_config', array());
    if (in_array('enable_css', $configs)) {
      drupal_add_css(drupal_get_path('module', 'ajax_select') .'/ajax_select.css');
    }
    $added = TRUE;
  }
}

/**
 * Prepare html for taxonomy
 *
 * @param $node
 *   Node object we had the ajax select to
 * 
 * @param $term
 *   Term object
 */
function _ajax_select_prepare_vocab_html(&$node, $term, $teaser, $page) {
  // TODO
  $new_html = theme('ajax_select_vocab_html', $node, $term);
  $node->ajax_select_vocab[$term->vid] = $new_html;
  /*
  if ($teaser) {
    $node->teaser .= $new_html;
  }
  else {
    $node->body .= $new_html;
  }
  */
}

function theme_ajax_select_vocab_html($node, $term) {
  $output .= drupal_get_form('ajax_select_vocab_form', $node, $term);
  return $output;
}

/**
 * Ajax form
 * 
 * The Ajax capable form that is displayed when viewing the node
 *
 * @param $node
 *   Node object we add the form to
 * 
 * @param $term
 *   Term object
 * 
 * @return form
 */
function ajax_select_vocab_form($node, $term) {
  $form = array();
  $id_base = 'ajax_select_vocab_'. $node->nid .'_'. $term->vid;
  $form['#id'] = form_clean_id($id_base .'_form');
  $form['#attributes'] = array('class' => 'ajax-select-form');
  $form['prefix'] = array(
    '#value' => '<div class="ajax-select ajax-select-vocab" id="'. form_clean_id($id_base) .'">'
  );
  $form['select'] = taxonomy_form($term->vid, $term->tid);
  $form['select']['#id'] = form_clean_id($id_base .'_select');
  $form['uri'] = array(
    '#type' => 'hidden',
    '#value' => url('ajax-select-vocab-submit/'. $node->nid .'/'. $term->vid, NULL, NULL, TRUE),
    '#id' => form_clean_id($id_base .'_uri')
  );
  $form['tid'] = array(
    '#type' => 'hidden',
    '#value' => $term->tid,
    '#id' => form_clean_id($id_base .'_tid')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#id' => form_clean_id($id_base .'_submit')
  );
  $form['suffix'] = array(
    '#value' => '</div>'
  );
  return $form;
}

function theme_ajax_select_vocab_form($form) {
  $output = '';
  $form['select']['#title'] = '';
  $output .= drupal_render($form['prefix']);
  $output .= drupal_render($form['select']);
  $output .= drupal_render($form['uri']);
  $output .= drupal_render($form['tid']);
  $output .= drupal_render($form['suffix']);
  return $output;
}

/**
 * Replace a field
 * 
 * Replace the field value by an Ajax capable select field
 *
 * @param $node
 *   Node object we had the ajax select to
 * 
 * @param $field_name
 *   Name of the field we had ajax capability to
 */
function _ajax_select_replace_field_html(&$node, $field_name, $teaser, $page) {
  $content_type = content_types($node->type);
  $field = $content_type['fields'][$field_name];
  $raw_html = theme('field', $node, $field, $node->$field_name, $teaser, $page);
  
  // For each field we replace the display of the its value(s) by the Ajax form
  foreach ($node->$field_name as $key => $value) {
    $node->{$field_name}[$key]['view'] = theme('ajax_select_field_html', $node, $field, $key, $value);
  }
  
  $new_html = theme('field', $node, $field, $node->$field_name, $teaser, $page);
  if ($teaser) {
    $node->teaser = str_replace($raw_html, $new_html, $node->teaser);
  }
  else {
    $node->body = str_replace($raw_html, $new_html, $node->body);
  }
}

function theme_ajax_select_field_html($node, $field, $delta, $node_field) {
  $output .= drupal_get_form('ajax_select_field_form', $node, $field, $delta, $node_field);
  return $output;
}

/**
 * Ajax form
 * 
 * The Ajax capable form that is displayed when viewing the node
 *
 * @param $node
 *   Node object we add the form to
 * 
 * @param $field
 *   Array containing the field information (got through content_types())
 * 
 * @param $delta
 *   Field widget delta (CCK)
 * 
 * @param $node_field
 *   Array containing the field value for this node
 * 
 * @return form
 */
function ajax_select_field_form($node, $field, $delta, $node_field) {
  $form = array();
  $id_base = 'ajax_select_field_'. $node->nid .'_'. $field['field_name'] .'_'. $delta;
  $form['#id'] = form_clean_id($id_base .'_form');
  $form['prefix'] = array(
    '#value' => '<div class="ajax-select ajax-select-field" id="'. form_clean_id($id_base) .'">'
  );
  $form['select'] = array(
    '#type' => 'select',
    '#options' => text_allowed_values($field),
    '#default_value' => $node_field['value'],
    '#id' => form_clean_id($id_base .'_select')
  );
  $form['uri'] = array(
    '#type' => 'hidden',
    '#value' => url('ajax-select-field-submit/'. $node->nid .'/'. $field['field_name'] .'/'. $delta, NULL, NULL, TRUE),
    '#id' => form_clean_id($id_base .'_uri')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#id' => form_clean_id($id_base .'_submit')
  );
  $form['suffix'] = array(
    '#value' => '</div>'
  );
  return $form;
}

function theme_ajax_select_field_form($form) {
  $output = '';
  // TODO: ?
  $output .= drupal_render($form['prefix']);
  $output .= drupal_render($form['select']);
  $output .= drupal_render($form['uri']);
  $output .= drupal_render($form['suffix']);
  return $output;
}

/**
 * Admin settings
 */
function ajax_select_admin_settings() {
  $form = array();
  // Configs
  $config_options = array(
    'enable_css' => t('Enable CSS'),
    //'enable_button' => t('Enable a submit button (otherwise it submit on change)'),
  );
  $form['ajax_select_config'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Configurations'),
    '#options' => $config_options,
    '#default_value' => variable_get('ajax_select_config', array()),
    '#description' => t(''),
  );
  
  // Taxonomy
  if (module_exists('taxonomy')) {
    $options_type = $options_vocab = array();
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $vid => $vocab) {
      $node_type_names = array();
      if (!empty($vocab->nodes) && !$vocab->tags && !$vocab->multiple) {
        foreach ($vocab->nodes as $node_type) {
          $node_type_names[$node_type] = node_get_types('name', $node_type);
          if (!array_key_exists($node_type, $options_type)) {
            $options_type[$node_type] = node_get_types('name', $node_type);
          }
        }
        $node_type_names = implode(', ', $node_type_names);
        $options_vocab[$vid] = t('[ @vocab_name ] - in %type_names', array('@vocab_name' => $vocab->name, '%type_names' => $node_type_names));
      }
    }
    $form['on_tax'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select Taxonomy vocabularies you want to enable ajax select on'),
      '#description' => t(''),
    );
    if (empty($options_type)) {
      $form['on_tax']['info'] = array('#type' => 'item', '#value' => t('There is no selectable Taxonomy field'));
    }
    else {
      $form['on_tax']['ajax_select_tax_types'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Content types'),
        '#options' => $options_type,
        '#default_value' => variable_get('ajax_select_tax_types', array()),
        '#description' => t(''),
      );
      $form['on_tax']['ajax_select_tax_vocabs'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Taxonomy vocabularies'),
        '#options' => $options_vocab,
        '#default_value' => variable_get('ajax_select_tax_vocabs', array()),
        '#description' => t(''),
      );
    }
  }
  
  // CCK
  if (module_exists('content')) {
    $options_type = $options_field = array();
    $content_types = content_types();
    // Prepare options
    foreach ($content_types as $type) {
      $has_field = FALSE;
      if (is_array($type['fields'])) {
        foreach ($type['fields'] as $field) {
          if (is_array($field['widget']) && $field['widget']['type'] == 'options_select') {
            $has_field = TRUE;
            if (array_key_exists($field['field_name'], $options_field)) {
              $options_field[$field['field_name']] .= t(', %type_name', array('%type_name' => $type['name']));
            }
            else {
              $options_field[$field['field_name']] = t('[ @field_name ]', array('@field_name' => $field['field_name']));
              $options_field[$field['field_name']] .= t(' - in %type_name', array('%type_name' => $type['name']));
            }
          }
        }
      }
      if ($has_field) {
        $options_type[$type['type']] = $type['name'];
      }
    }
    $form['on_cck'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select CCK fields you want to enable ajax select on'),
      '#description' => t(''),
    );
    if (empty($options_type)) {
      $form['on_cck']['info'] = array('#type' => 'item', '#value' => t('There is no selectable CCK field'));
    }
    else {
      $form['on_cck']['ajax_select_cck_types'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Content types'),
        '#options' => $options_type,
        '#default_value' => variable_get('ajax_select_cck_types', array()),
        '#description' => t(''),
      );
      $form['on_cck']['ajax_select_cck_fields'] = array(
        '#type' => 'checkboxes',
        '#title' => t('CCK fields'),
        '#options' => $options_field,
        '#default_value' => variable_get('ajax_select_cck_fields', array()),
        '#description' => t(''),
      );
    }
  }
  
  $form['array_filter'] = array('#type' => 'hidden');
  return system_settings_form($form);
}
